package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

type WebhookController struct {
	session *mgo.Session // Controller must have access to MongoDB session
}

/*
Passes pointer to WebhookController to avoid sending struct
*/
func NewWebhookController(s *mgo.Session) *WebhookController {
	return &WebhookController{s}
}

/*
Registers a new webhook in the database
*/
func (wh WebhookController) RegisterWebhook(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// TODO: if base currency != EUR, inform of not yet implemented

	// 1. Store JSON body in a Webhook object
	hook := Webhook{}
	json.NewDecoder(r.Body).Decode(&hook)

	// Give webhook BSON ID
	hook.ID = bson.NewObjectId()

	// 3. Store object in database
	wh.session.DB("heroku_0mcqcr8h").C("webhooks").Insert(hook)

	fmt.Fprintln(w, hook.ID.Hex())
}

/*
Displays a specific webhook
*/
func (wh WebhookController) DisplayWebhook(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Gets id from URL
	wId := p.ByName("id")

	// Check if path variable is actual ObjectId hex representation
	if !bson.IsObjectIdHex(wId) {
		fmt.Fprintln(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	// Create empty instance of Webhook
	hook := Webhook{}
	// Find webhook in database, if it exists
	err := wh.session.DB("heroku_0mcqcr8h").C("webhooks").FindId(bson.ObjectIdHex(wId)).One(&hook)
	if err != nil {
		fmt.Fprintln(w, "Couldn't find webhook", http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&hook)
}

/*
Removes a specific webhook from the database
*/
func (wh WebhookController) RemoveWebhook(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Gets id from URL
	wId := p.ByName("id")

	// Check if path variable is actual ObjectId hex representation
	if !bson.IsObjectIdHex(wId) {
		fmt.Fprintln(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	err := wh.session.DB("heroku_0mcqcr8h").C("webhooks").RemoveId(bson.ObjectIdHex(wId))
	if err != nil {
		fmt.Fprintln(w, "Could not find webhook to delete", http.StatusNotFound)
	}
}

/*
Gets the latest exchange rates with base EUR and stores them in DB
*/
func (wh WebhookController) GetLatestRates() {
	// Retrieves exchange rates with base EUR
	resp, err := http.Get("http://api.fixer.io/latest?base=EUR")
	if err != nil {
		log.Fatal(err)
		// TODO: legg inn timeout
		return
	}
	defer resp.Body.Close()

	// currData stores all fields from fixer.io URL
	var currData map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&currData)

	// Stores currData in DB
	wh.session.DB("heroku_0mcqcr8h").C("fixer").Insert(currData)

	var ratesData map[string]map[string]float64
	json.NewDecoder(resp.Body).Decode(&ratesData)
	wh.CheckWebhooks(ratesData)
}

/*
Retrieves webhooks from database and checks them against todays exchange rates
Invokes a webhook if its min/max trigger values are met
*/
func (wh WebhookController) CheckWebhooks(currencyData map[string]map[string]float64) {
	// 1. Retrieve webhooks from DB and store in map
	hooks := []Webhook{}
	wh.session.DB("heroku_0mcqcr8h").C("webhooks").Find(nil).All(&hooks)

	// 3. Check every webhook's min/max trigger value against today's value
	// and send response if trigger values are reached
	for _, hook := range hooks {
		var currencyValue = currencyData["rates"][hook.TargetCurrency]
		if currencyValue < hook.MinTriggerValue || currencyValue > hook.MaxTriggerValue {
			//send msg to webhook url
			invokeHook := InvokedWebhook{
				hook.BaseCurrency,
				hook.TargetCurrency,
				currencyValue,
				hook.MinTriggerValue,
				hook.MaxTriggerValue,
			}
			b := new(bytes.Buffer)
			json.NewEncoder(b).Encode(invokeHook)
			resp, err := http.Post(hook.WebhookURL, "application/json", b)
			if err != nil {
				log.Fatal(err)
				return
			}
			if resp.StatusCode == 200 || resp.StatusCode == 204 {
				log.Println("Invoked webhook %s", hook.WebhookURL)
			} else {
				log.Println("Failed to invoke webhook %s (%d)", hook.WebhookURL, resp.StatusCode)
			}
		}
	}
}
