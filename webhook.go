package main

import "gopkg.in/mgo.v2/bson"

type Webhook struct {
	ID              bson.ObjectId `json:"-" bson:"_id"`
	WebhookURL      string        `json:"webhookURL" bson:"webhookurl"`
	BaseCurrency    string        `json:"baseCurrency" bson:"basecurrency"`
	TargetCurrency  string        `json:"targetCurrency" bson:"targetcurrency"`
	MinTriggerValue float64       `json:"minTriggerValue" bson:"mintriggervalue"`
	MaxTriggerValue float64       `json:"maxTriggerValue" bson:"maxtriggervalue"`
}

type InvokedWebhook struct {
	BaseCurrency    string  `json:"baseCurrency" bson:"basecurrency"`
	TargetCurrency  string  `json:"targetCurrency" bson:"targetcurrency"`
	CurrentRate     float64 `json:"currentRate"`
	MinTriggerValue float64 `json:"minTriggerValue" bson:"mintriggervalue"`
	MaxTriggerValue float64 `json:"maxTriggerValue" bson:"maxtriggervalue"`
}
