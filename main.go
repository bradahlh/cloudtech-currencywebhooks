package main

import (
	"github.com/jasonlvhit/gocron"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"net/http"
	"os"
)

func main() {
	router := httprouter.New()

	// Create two controllers who handles all actions related to them
	whController := NewWebhookController(getSession())
	cController := NewCurrencyController(getSession())

	// Method and path decides functionality
	router.GET("/exchange/:id", whController.DisplayWebhook)
	router.POST("/exchange", whController.RegisterWebhook)
	router.POST("/exchange/latest", cController.GetTwo)
	//router.POST("/exchange/average", cController.Average)
	router.DELETE("/exchange/:id", whController.RemoveWebhook)

	host := os.Getenv("HOST")
	port := os.Getenv("PORT")

	//http.ListenAndServe(":8080", router)
	http.ListenAndServe(host+":"+port, router)

	/*
	Gets latest exchange rates every 24 hours. Code inspired by:
	https://stackoverflow.com/questions/19549199/golang-implementing-a-cron-executing-tasks-at-a-specific-time
	*/
	scheduler := gocron.NewScheduler()
	scheduler.Every(24).Hours().Do(whController.GetLatestRates)
	<-scheduler.Start()
}

/*
Returns pointer to session in use
 */
func getSession() *mgo.Session {
	mongoDbUri := os.Getenv("MONGODB_URI")
	session, err := mgo.Dial(mongoDbUri)
	if err != nil {
		panic(err)
	}
	return session
}
