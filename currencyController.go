package main

import (
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"net/http"
)

type CurrencyController struct {
	session *mgo.Session // Controller must have access to MongoDB session
}

// Passes pointer to CurrencyController to avoid sending the whole struct
func NewCurrencyController(s *mgo.Session) *CurrencyController {
	return &CurrencyController{s}
}

/*
User posts JSON body and gets the latest exchange rate between two chosen currencies
*/
func (cc CurrencyController) GetTwo(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// Retrieve which currencies to check from JSON body
	type ExchangeRate struct {
		Base   string `json:"baseCurrency"`
		Target string `json:"targetCurrency"`
	}
	rate := ExchangeRate{}
	json.NewDecoder(r.Body).Decode(&rate)

	// Create URL for fixer.io
	fixerURL := "http://api.fixer.io/latest?base=" + rate.Base + ";symbols=" + rate.Target
	resp, err := http.Get(fixerURL)
	if err != nil {
		fmt.Fprintln(w, "Could not retrieve page at the moment.", http.StatusNotFound)
		return
	}
	defer resp.Body.Close()

	// exchData stores all fields from fixer.io URL
	exchData := make(map[string]map[string]interface{})
	json.NewDecoder(resp.Body).Decode(&exchData)
	// Prints nested map value to response writer
	fmt.Fprintln(w, exchData["rates"][rate.Target])
}

//func (cc CurrencyController) Average(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
//
//}
